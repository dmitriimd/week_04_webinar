package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.*;

public class CompetitionImpl implements Competition{

    private long id = 0;

    private Map<Long, MyParticipant> participantMap = new HashMap<>();

    private Set<Athlete> registeredAthets = new HashSet<>();

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        if(registeredAthets.contains(participant)){
            throw new IllegalStateException("Duplicate registration");
        }
        registeredAthets.add(participant);
        MyParticipant internalAndSecuredObject = new MyParticipant(++id);

        participantMap.put(internalAndSecuredObject.id, internalAndSecuredObject);

        return internalAndSecuredObject.getClone();
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        MyParticipant myParticipant = participantMap.get(id);
        if(myParticipant != null){
            //то обновляем
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score); //делигируем выполнение на метод updateScore(long id, long score)
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        LinkedList<Participant> myParticipants = new LinkedList<>(participantMap.values());
        myParticipants.sort(((o1, o2) -> 0));
        return myParticipants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        return null;
    }

    private static class MyParticipant implements Participant {
        public final Long id;
        private long score;

        public MyParticipant(long id) {
            this.id = id;
        }

        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация о спортсмене
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return null;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        public void incrementScore(long value){
            score += value;
        }

        public MyParticipant getClone() {
            return new MyParticipant(this.id);
        }
    }
}
