package ru.edu.screens;

import ru.edu.Competition;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class MainScreen implements Screen {
    private InputStream input;
    private PrintStream output;


    public MainScreen(InputStream in, PrintStream out) {
        input = in;
        output = out;
    }

    /**
     * Выводит текст на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        output.println("Выберите действие:");
        output.println("1) Регистрация участника");
        output.println("2) Личный зачет");
        output.println("3) Командный зачет");
        output.println("4) Список зарегистрированных участников");
        output.println("0) Выход");

    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @return
     * @param competition
     */
    @Override
    public Screen readInput(Competition competition) {
        Scanner scanner = new Scanner(input);

        int answer = scanner.nextInt();
        switch (answer){
            case 1:
                return new RegisterScreen(input, output);
                //другие варианты ответа
            case 0:
                return null;
            default:
                return this;
        }
    }
}
