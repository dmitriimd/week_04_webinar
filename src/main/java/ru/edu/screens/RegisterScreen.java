package ru.edu.screens;

import ru.edu.Competition;
import ru.edu.model.AthelteImpl;
import ru.edu.model.Participant;

import java.io.InputStream;
import java.io.PrintStream;

public class RegisterScreen implements Screen {
    private int step = 0;
    private String[] promptsForSteps = {
            "Введите имя: ", //step = 0
            "Введите фамилию: " //step = 1
            //...
    };

    private PrintStream output;

    private AthelteImpl.Builder athleteForRegistation = null;

    public RegisterScreen(InputStream input, PrintStream output) {
    }

    /**
     * Выводит текст на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        if(step == 0){
            athleteForRegistation = AthelteImpl.builder();
        }
        output.println(promptsForSteps[step]);
    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @return
     * @param competition
     */
    @Override
    public Screen readInput(Competition competition) {
        String input = null; // из сканера in.readline()
        if(step == 0){
            athleteForRegistation.setFirstName(input);
        }
        if(step == 1){ //пускай это последний шаг
            athleteForRegistation.setLastName(input);

            AthelteImpl registerAthlete = athleteForRegistation.build();
            try {
                Participant registered = competition.register(registerAthlete);
                output.println("Зарегистрировали участника #"+registered.getId()/* + " имя: "+...  */);
            }catch (IllegalArgumentException e){
                output.println("Ошибка: "+ e.getMessage());
            }
            return null; //возвращаемся на предыдущий экран
        }
        step++;
        return this;//остаемся на экране
    }
}
