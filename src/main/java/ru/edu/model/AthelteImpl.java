package ru.edu.model;

public class AthelteImpl implements Athlete{
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return null;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return null;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return null;
    }

    public static class Builder {
        public Builder setFirstName(String input) {
            return this;
        }

        public Builder setLastName(String input) {
            return this;
        }

        public AthelteImpl build() {
            return null;
        }
    }

    //не забываем equals & hashCode
}
